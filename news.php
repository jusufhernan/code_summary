<?php 

include 'koneksi.php';

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Consolution - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">
    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>


<section class="ftco-section bg-light">
			<div class="container">
				<div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-6 text-center heading-section ftco-animate">
            <h2 class="mb-4"><span>Recent</span> Blog</h2>
            <!-- <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p> -->
          </div>
        </div>
				<div class="row">

          <?php $ambil = $koneksi->query("SELECT * FROM blog"); ?>
          <?php while($perproduk = $ambil->fetch_assoc()) { ?>
          <br>
          <div class="col-md-4">
            <div class="blog-entry">
              <a href="#" class="block-20 d-flex align-items-end"><img src="images/foto_blog/<?php echo $perproduk['foto_blog']; ?>" alt""></a>
								<!-- <div class="meta-date text-center p-2">
                  <span class="day">26</span>
                  <span class="mos">June</span>
                  <span class="yr">2019</span>
                </div> -->
              <!-- </a> -->
              <div class="text bg-white p-4">
                <h3 class="heading"><?php echo $perproduk['title']; ?></h3>
                <p><?php echo $perproduk['Deskripsi']; ?></p>
                <div class="d-flex align-items-center mt-4">
	                <p class="mb-0"><a href="#" class="btn btn-primary">Read More <span class="ion-ios-arrow-round-forward"></span></a></p>
	                <p class="ml-auto mb-0">
	                	<a href="#" class="mr-2">Admin</a>
	                	<a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a>
	                </p>
                </div>
              </div>
            </div>
            
          </div>
            <?php } ?>
          
              </div>
            </div>
          </div>
        </div>
			</div>
		</section>


  </body>
</html>